// To ensure that elements are ready on polyfilled browsers, 
// wait for WebComponentsReady. 
document.addEventListener('WebComponentsReady', function() {
  var input = document.querySelector('paper-input');
  var button = document.querySelector('paper-button');
  var greeting = document.getElementById("greeting");
  
  button.addEventListener('click', function() {
    requestData();
    greeting.textContent = 'Hello, ' + input.value;
  });       
});

function requestData(){
  //URL para la peticion Ajax
  var url = "https://www.googleapis.com/youtube/v3/search";
  //https://elements.polymer-project.org/bower_components/iron-list/demo/data/contacts.json;
  //Obtenemos una ref al elemento iron-ajax
  var ajax = document.querySelector('iron-ajax');
  //Agregamos un listener para manejar el evento response
  ajax.addEventListener('response', processResponse);
  //Disparamos la peticion
  if (ajax.url == "") {
    ajax.url = url;  
  } else {
    ajax.generateRequest();
  }
}

function processResponse(resp) {
  console.log("On response");
  console.log(resp.detail.response.items);
}