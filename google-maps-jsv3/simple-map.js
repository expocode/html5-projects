var map;
var myPos;
var marker;

function handleNoGeolocation(errorFlag) {
   //Esta función sencillamente levanta una ventana de información definida con el objeto " google.maps.InfoWindow(options)"  
   //indicando el tipo error obtenido. 
   if (errorFlag) {
     var content = 'Error: Fallo el servicio de Geolocalización.';
   } else {
     var content = 'Error: Su browser no soporta Geolocalización.';
   }
 
   var options = {
     map: map,
     position: new google.maps.LatLng(60, 105),
     content: content
   };
 
   var infowindow = new google.maps.InfoWindow(options);
   map.setCenter(options.position);
}

//También debemos definir la función showInfo la cual muestra la ventana con le mensaje "Ubicación Actual".
function showInfo() {
	//Aumentar el zoom del mapa
	map.setZoom(16);
	map.setCenter(marker.getPosition());
 
	//Construir una ventana de información
	var contentString = 'Ubicación Actual';
	var infowindow = new google.maps.InfoWindow({
		content: contentString
	});
	//Abrir la ventana de información
	infowindow.open(map,marker);
}

function newMarker(markerData){
	var posSitio = new google.maps.LatLng(markerData.lt,markerData.ln);
	var sitio = new google.maps.Marker({
		map:map,
		draggable:false,
		animation: google.maps.Animation.DROP,
		position: posSitio
	});
	//id="content"
	var content = "<div><h4>" + markerData.titulo + "</h4>" + markerData.descrip + "</div>"; 
	var infowindow = new google.maps.InfoWindow({ content: content }); 
	google.maps.event.addListener(sitio, 'click', function(){ infowindow.open(map,sitio); }); 
}

function showSitios() {
	//hacemos una peticion asincrona de tipo GET para obtener el fichero JSON
	var jqxhr = $.getJSON( "data/sitios_turisticos.json", function(data) {
		//En el caso de obtener el fichero con éxito, entonces iteramos 
		//sobre el objeto json obtenido
		console.log( "success" ); 
		for (i=0; i < data.length; i++) {
		//Obtenemos cada obtejo JSON el cual contiene la información del sitio turistico
			var sitio = data[i];
			//Llamamos a la función newMarker para insertar un Marker indicando 
			//la localización del sitio turistico
			newMarker(sitio);
		}
	}).done(function() { 
		console.log( "second success" ); 
	}).fail(function() { 
		console.log( "error" ); 
	}).always(function() { 
		console.log( "complete" ); 
	});
}

function initialize() {
	var mapOptions = {
	    zoom: 16,
	    center: new google.maps.LatLng(-34.397, 150.644),
	    mapTypeId: google.maps.MapTypeId.ROADMAP
	};
	map = new google.maps.Map(document.getElementById('mapa'), mapOptions);

	//Geolocalización con HTML5 
	//Primero verificamos si el navegador soporta Geolocalización.
	if(navigator.geolocation) {
	  //Solicitamos al navegador la localización actual del usuario con el método getCurrentPosition. 
	  navigator.geolocation.getCurrentPosition(function(position) { 
		  /* Como el método getCurrentPosition() ea asíncrona requiere de una función   
		   * que será llamada en el momento en que se obtenga los datos de la ubicación.
		   */
		 
			//Definimos un objeto de localización con el objeto google.maps.LatLng y las coordenadas obtenidas del navegador.
			var pos = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);

			//Definimos una ventana de información que señale la ubicación actual obtenida con Geolocalización HTML5.
			var infowindow = new google.maps.InfoWindow({
				map: map,
				position: pos,
				content: 'Ubicación encontrada empleando HTML5.'
			});

			//Para definir un Marker, creamos una variable con referencia al objeto "google.maps.Marker"
			//y le pasamos un objeto JavaScript con las propiedades map, draggable, animation, y position.
			marker = new google.maps.Marker({
				map:map,
				draggable:false,
				animation: google.maps.Animation.DROP,
				position: pos
			});

			//Ågregamos un Listener (Escuchador) del evento clic al Marker, 
			//el cual dispara una función que muestra una Ventana de información. 
			google.maps.event.addListener(marker, 'click', showInfo);

			//Ågregamos un Listener (Escuchador) del evento doble clic al Marker, 
			//el cual dispara una función que dibuja los marcadores de los 
			//sitios turisticos definidos en el fichero sitios-turisticos.json
			google.maps.event.addListener(marker, 'dblclick', showSitios);

			//Centramos el mapa en la ubicación obtenida.
			map.setCenter(pos);

	  }, function() { 
	      //En el caso de que el servicio de localización falle llamamos a la función handleNoGeolocation
	      // con el parámetro true para indicar al usuario de que hubo un error en el servicio de geolocalización.
	      handleNoGeolocation(true); 
	  });
	} else {
	   // En el caso de que el Browser no soporta Geolocalización llamamos a la función handleNoGeolocation
	   // con el parámetro false.
	   handleNoGeolocation(false);
	}
}	

google.maps.event.addDomListener(window, 'load', initialize);



 
